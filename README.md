# Music Downloader Desktop

**Clone and run.**

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://teunissenstefan@bitbucket.org/teunissenstefan/music-downloader-desktop.git
# Go into the repository
cd music-downloader-desktop
# Install dependencies
npm install
# Run the app
npm start
```