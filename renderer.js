const remote = require('electron').remote;
const fs = require('fs');
const youtubedl = require('youtube-dl');
const ffmpeg = require('ffmpeg');

function Close(){
    if(queue.length == 0){
        doClose();
    }else{
        if (confirm('There are still files in the queue, are you sure you want to quit?')) {
            doClose();
        }
    } 
}

function doClose(){
    fs.readdir('./', (err, files) => {
        files.forEach(file => {
            if(file.endsWith(".mp4")){
                fs.unlinkSync(file);
            }
        });
        var window = remote.getCurrentWindow();
        window.close(); 
    })
}

function Minimize(){
    var window = remote.getCurrentWindow();
    window.minimize();  
}

function ShowStatus(statuscmd){
    switch(statuscmd){
        case "STARTING":
            status = "Starting download";
            break;
        case "STARTED":
            status = "Downloading";
            break;
        case "CONVERTING":
            status = "Converting";
            break;
        case "DONE":
            status = "Done";
            break;
        case "GETTINGINFO":
            status = "Getting information";
            break;
        case "WAITING":
            status = "Waiting";
            break;
        case "ERROR":
            status = "ERROR";
            break;
        default:
            status = '';
            break;
    }
    return status;
}

var queue = [];
var done = [];

UpdateLists();
function UpdateLists(){
    //Lists
    queue.forEach(function(entry,index,object) {
        if(entry['status'] == "DONE"){
            done.unshift(entry);
            object.splice(index,1);
        }
        document.getElementById("queueContent").innerHTML += "<div class='songInList'>"+entry['title']+"<br />Status: "+ShowStatus(entry['status'])+"</div>";
    });

    //Visual
    document.getElementById("queueContent").innerHTML = "";
    document.getElementById("doneContent").innerHTML = "";
    queue.forEach(function(entry) {
        if(entry['status']=="STARTED"){
            document.getElementById("queueContent").innerHTML += "<div class='songInList'>"+entry['title']+"<br />"+ShowStatus(entry['status'])+" "+entry['percent']+"%</div>";
        }else{
            document.getElementById("queueContent").innerHTML += "<div class='songInList'>"+entry['title']+"<br />"+ShowStatus(entry['status'])+"</div>";
        }
    });
    done.forEach(function(entry) {
        document.getElementById("doneContent").innerHTML += "<div class='songInList'>"+entry['title']+"</div>";
    });
}

function SetStatus(id,status,percent){
    for (var i in queue) {
     if (queue[i]['id'] == id) {
        queue[i]['status'] = status;
        queue[i]['percent'] = percent;
        break;
     }
   }
    for (var i in done) {
     if (done[i]['id'] == id) {
        done[i]['status'] = status;
        break;
     }
   }
}
////Checken of er een aan het downloaden is
//Ja: wachten tot die klaar is met alles (ook converteren)
//Nee: starten

function DownloadFunction(url, fileToConvert, id,songtitle){
    var size = 0;

    var video = youtubedl(url,
        ['-f bestaudio'],
        { cwd: __dirname });

    video.on('info', function(info) {
        size = info.size;
        console.log('Download started');
        SetStatus(id,'STARTED',0);
        UpdateLists();
    });

    var pos = 0;
    video.on('data', function data(chunk) {
        'use strict';
        pos += chunk.length;

        if (size) {
            var percent = (pos / size * 100).toFixed(2);
            SetStatus(id,'STARTED',percent);
        }
        UpdateLists();
    });
    

    video.on('end', function() {
        console.log('Finished downloading!' + fileToConvert);
        SetStatus(id,'CONVERTING',0);
        UpdateLists();
        ConvertToMp3(fileToConvert,songtitle,id);
    });

    video.pipe(fs.createWriteStream(fileToConvert.replace(/ /g,'')));
    UpdateLists();
}

function Download(){
    if(!fs.existsSync('music')){
        fs.mkdirSync('music');
    }

    var url = document.getElementById('urlTextbox').value;
    document.getElementById('urlTextbox').value = "";

    youtubedl.getInfo(url, function(err, info) {
        if (err) throw err;
        
        queue.unshift({id: info.id, title: info.title, url: url, status: "WAITING", fileToConvert: info._filename, size: info.size, percent: 0});
        UpdateLists();
        NextDownload();
    });
}

function NextDownload(){
    var url = '';
    var anotherOne = false;
    var fileToConvert = '';
    var id = '';
    var title = '';
    for (var i in queue) {
        console.log(queue[i]);
        if (queue[i]['status'] == "WAITING") {
            
            url = queue[i]['url'];
            fileToConvert = queue[i]['fileToConvert'];
            id = queue[i]['id'];
            title = queue[i]['title'];
            anotherOne = true;
            break;
        }
    }
    if(anotherOne){
        console.log("Start next download");
        DownloadFunction(url,fileToConvert,id,title);
    }
    UpdateLists();
}

function ConvertToMp3(fileToConvert,title,id){
    try {
        fileToConvert = fileToConvert.replace(/ /g,'');
        console.log(fileToConvert);
        console.log(title);
        var process = new ffmpeg(fileToConvert);
        console.log(process);
        process.then(function (video) {
            console.log(video);
            title = title.replace(/['"]+/g, '');
            title = title.replace(/[|]/g,' ');
            video.fnExtractSoundToMP3('"music/'+title+'.mp3"', function (error, file) {
                if (!error){
                    console.log('Audio file: ' + file);
                    SetStatus(id,'DONE',0);
                    UpdateLists();
                    fs.unlinkSync(fileToConvert);
                }else{
                    console.log('Error: '+error);
                    SetStatus(id,'ERROR',0);
                    UpdateLists();
                }
                UpdateLists();
            });
        }, function (err) {
            console.log('Error: ' + err);
        });
    } catch (e) {
        console.log(e.code);
        console.log(e.msg);
        SetStatus(id,'ERROR',0);
    }
    UpdateLists();
}

function OpenFolder(){
    var open = require("open");
    open("music");
}

document.getElementById("urlTextbox")
    .addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        document.getElementById("downloadBtn").click();
        UpdateLists();
    }
});

document.getElementById("downloadBtn")
    .addEventListener("click", function(event) {
        Download();
});

document.getElementById("openFolderButton")
    .addEventListener("click", function(event) {
        OpenFolder();
});

document.getElementById("closeButton")
    .addEventListener("click", function(event) {
        Close();
});

document.getElementById("minButton")
    .addEventListener("click", function(event) {
        Minimize();
});